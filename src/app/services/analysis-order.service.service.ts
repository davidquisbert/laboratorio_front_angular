import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { environment } from '../../environments/enviroment';

const headers = new HttpHeaders();
headers.set("Access-Control-Allow-Origin", "*");
headers.set("Content-Type", "application/json");

@Injectable({
  providedIn: 'root'
})
export class AnalysisOrderServiceService {
  currentDate: Date;

  constructor( private http: HttpClient ) {
    this.currentDate = new Date();

  }

  getAnalysis(id: number) {
    return this.http.get('api/order_analysis/complete/' + id);
  }


  registerOrder(observacion: string,idTipoEntrega: number) {
    const url = `${environment.urlApi}/api/order_analysis`;
    const params = {
      observacion: observacion,
      estado: 1,
      fecha: this.currentDate,
      idTipoEntrega: idTipoEntrega,
      idDoctor: 1,
      idEmployee: 1,
    };
    return this.http.post(url, params);
  }
  registerAnalysis(idOrdenAnalysis: number,idAnalisis: number) {
    const url = `${environment.urlApi}/api/analysis`;
    const params = {
      idOrdenAnalysis: idOrdenAnalysis,
      idAnalisis: idAnalisis,
    };
    return this.http.post(url, params);
  }

}
