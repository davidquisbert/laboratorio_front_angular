import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './pages/login/login.component';
import { IndexHomeComponent } from './pages/home/index/index-home.component';

const routes: Routes = [
  //home
  { path: '', component: IndexHomeComponent},
  //What to do
  { path: 'login', component: LoginComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
