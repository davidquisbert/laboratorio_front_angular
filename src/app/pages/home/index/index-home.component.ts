import {Component, OnInit} from '@angular/core';
import {AnalysisOrderServiceService} from "../../../services/analysis-order.service.service";
import {RespuestaApi} from "../../../interfaces/interfaces";

@Component({
  selector: 'app-index-home',
  templateUrl: './index-home.component.html',
  styleUrls: ['./index-home.component.css']
})
export class IndexHomeComponent implements OnInit{

  analisis: any;
  tipoEntrega = '';
  observacion = '';
  numeroAnalisis = 0;
  constructor(
    private analysisService: AnalysisOrderServiceService
  ) {
  }

  ngOnInit() {

  }

  getAllAnalysis() {
    this.analysisService.getAnalysis(this.numeroAnalisis)
      .subscribe(
        (response: RespuestaApi<any>) => {
          if (response.codigo === 0) {
            this.analisis = response.data;
            if (this.analisis.idTipoEntrega == 1) {
              this.tipoEntrega = 'Envio por parte del Centro Médico';
            } else {
              this.tipoEntrega = 'Recojo en el Centro Médico';

            }
          }

          this.observacion = this.analisis.observacion;
        },
        err => {
          console.log(err);
        }
      );
  }

}
